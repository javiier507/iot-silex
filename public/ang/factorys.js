angular
.module('app')
.factory('Articulos', ['$http', Articulos])
.factory('Autores', ['$http', Autores])

function Articulos()
{
	return [
		{
			titulo: "Prueba #1",
			descripcion: "Sonrian pinches pendejos!",
			imagen: "portada-smile.jpg"
		},
		{
			titulo: "Prueba #2",
			descripcion: "Y vos cuantas copas tenes! seguros sos un nedm (un argentino)",
			imagen: "portada-monkey.jpg"
		}
	];	
}

function Autores($http)
{
	return [
		{
			nombre: "Carlos Peñalba",
			descripcion: "Me gusta los azados con ceveza",
			imagen: "perfil-zorro.jpg"
		},
		{
			nombre: "Christopher Miranda",
			descripcion: "Sin descripcion de momento",
			imagen: "perfil-oso.jpg"
		},
		{
			nombre: "Kisna Quiroz",
			descripcion: "Sin descripcion de momento",
			imagen: "perfil-pantera.jpg"
		},
		{
			nombre: "Jorge Aguilar",
			descripcion: "Sin descripcion de momento",
			imagen: "perfil-mapache.jpg"
		}
	];
}