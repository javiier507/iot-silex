<?php
namespace Appsilex;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__.'/../models/Database.php';

class ArticleController
{
    private $database;

    public function __construct()
    {
        $this->database = Database::getInstancia();
    }

    private function getArticles()
    {
        return $this->database->readArticles();
    }

    public function index(Application $app)
    {
        return $app['twig']->render('blog/article-index.twig', ['articles' => $this->getArticles()]);
    }

    public function admin(Application $app)
    {
        return $app['twig']->render('admin/article-index.twig', ['articles' => $this->getArticles()]);  
    }

    public function show(Application $app, $id)
    {
        $article = $this->database->showArticle($id);
        return $app['twig']->render('blog/article-show.twig', compact('article'));
    }

    public function create(Application $app)
    {   
        return $app['twig']->render('admin/article-create.twig');
    }

    public function store(Application $app, Request $request)
    {
        if($request->get('create'))
        {
            $category = $request->get('category');
            $image = "concepto.jpg";
            
            if($category=="maquina-a-maquina") 
            {
                $image = "m2m.jpg";    
            }
            elseif($category=="ipv6")
            {
                $image = "ipv6.jpg";
            }
            elseif($category=="seguridad") 
            {
                $image = "seguridad.jpg";
            }

            $title = $request->get('title');
            $content = $request->get('content');
            $user_id = $app['session']->get('user')['user_id'];

            $article = ['title' => $title, 'content' => $content, 
                'category' => $category, 'image' => $image, 'user_id' => $user_id];
            $article = (object)$article;
            $operacion = $this->database->createArticle($article);

            return $app->redirect('/administracion');
        }
        else
        {
            return $app->redirect('/articulo-nuevo');
        }        
    }

    public function destroy(Application $app, $id)
    {
        $operacion = $this->database->deleteArticle($id);
        return json_encode(['operacion' => $operacion]);
    }
}