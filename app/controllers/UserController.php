<?php
namespace Appsilex;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class UserController
{
    private $database;

    public function __construct()
    {
        $this->database = Database::getInstancia();
    }

    public function index(Application $app)
    {
        $users = $this->database->readUsers();
        return $app['twig']->render('blog/user-index.twig', compact('users'));
    }

    public function getLogin(Application $app)
    {
        return $app['twig']->render('auth/login.twig');
    }

    public function postLogin(Application $app, Request $request)
    {
        if($request->get('login'))
        {
            $username_email = $request->get('username_email');
            $password = $request->get('password');

            $user = ['username' => $username_email, 'email' => $username_email, 'password' => $password];
            $user = (object)$user;
            $operacion = $this->database->findUser($user);

            if($operacion==1)
            {
                $credentials = $this->database->getCredentialsUser($username_email);
                $app['session']->set('user', ['user_id' => $credentials['id'], 
                    'username' => $credentials['username']]);                
                return $app->redirect('/administracion');
            }
            else
            {
                return $app->redirect('/iniciar-sesion');
            }
        }
    }

    public function logout(Application $app)
    {
        $app['session']->clear();
        return $app->redirect('/');
    }

    public function ubication(Application $app)
    {
        return $app['twig']->render('blog/ubication.twig');
    }
}