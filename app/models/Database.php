<?php
namespace Appsilex;
class Database
{
    private static $instancia;
    private $conexion;

    private function __construct()
    {
        try
        {
            $opciones = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $this->conexion = new \PDO('mysql:host=localhost;dbname=iot', 'root', '', $opciones);
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en la conexion');
        }
    }

    public static function getInstancia()
    {
        if(!self::$instancia)
        {
            self::$instancia = new self();
        }
        return self::$instancia;
    }

    public function __clone()
    {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }

    private function getConexion()
    {
        return $this->conexion;
    }

    private function cerrarConexion()
    {
        //$this->conexion = null;
    }

    public function createArticle($article)
    {
        $operacion = 0;
        try
        {
            $query = 'INSERT INTO articles (title, content, category, image, user_id) VALUES (?,?,?,?,?)';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->bindParam(1, $article->title);
            $sentencia->bindParam(2, $article->content);
            $sentencia->bindParam(3, $article->category);
            $sentencia->bindParam(4, $article->image);
            $sentencia->bindParam(5, $article->user_id);
            $sentencia->execute();
            $operacion = $sentencia->rowCount();
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Crear Articulo');
        }
        return $operacion;
    }

    public function readArticles()
    {
        $registros = null;
        try
        {
            $query = 'select articles.id, articles.title, articles.content, articles.category, 
                    articles.image, users.username from articles inner join users 
                    on articles.user_id = users.id order by articles.id desc';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->execute();
            $registros = $sentencia->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Leer Articulos');
        }
        return $registros;
    }

    public function showArticle($id)
    {
        $registros = null;
        try
        {
            $query = 'select articles.title, articles.content, articles.category, articles.image, 
                    users.username from articles inner join users on articles.user_id = users.id 
                    WHERE articles.id = ?';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->bindParam(1, $id);
            $sentencia->execute();
            $registros = $sentencia->fetch(\PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Mostrar Articulos');
        }
        return $registros;
    }

    public function deleteArticle($id)
    {
        $operacion = 0;
        try
        {
            $query = 'DELETE FROM articles WHERE id = ?';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->bindParam(1, $id);
            $sentencia->execute();
            $operacion = $sentencia->rowCount();
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Eliminar Articulo');
        }
        return $operacion;
    }

    public function readUsers()
    {
        $registros = null;
        try
        {
            $query = 'select firstname, lastname, image, phrase from users order by id';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->execute();
            $registros = $sentencia->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Leer Usuarios');
        }
        return $registros;
    }

    public function findUser($user)
    {
        $operacion = 0;
        try
        {
            $query = 'select count(id) as operacion from users where ( username = ? or email = ? ) 
            and password = ?';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->bindParam(1, $user->username);
            $sentencia->bindParam(2, $user->email);
            $sentencia->bindParam(3, $user->password);
            $sentencia->execute();
            $registro = $sentencia->fetch(\PDO::FETCH_ASSOC);
            $operacion = $registro['operacion'];
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Encontrar Usuario');
        }
        return $operacion;
    }

    public function getCredentialsUser($username_email)
    {
        $registros = null;
        try
        {
            $query = 'select id, username from users WHERE username = ? or email = ?';
            $sentencia = $this->conexion->prepare($query);
            $sentencia->bindParam(1, $username_email);
            $sentencia->bindParam(2, $username_email);
            $sentencia->execute();
            $registros = $sentencia->fetch(\PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            throw new PDOException('Error en Obtener Credenciales de Usuario');
        }
        return $registros;
    }
}