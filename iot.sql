-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-11-2015 a las 04:03:46
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `iot`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articles`
--

INSERT INTO `articles` (`id`, `title`, `content`, `category`, `image`, `user_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Concepto de Internet de las Cosas', '<div>El Internet de las cosas, “Internet of the things (IoT)” o “Internet of Everything (IoE)”, es la interconexión digital de objetos cotidianos al internet. En este punto se podría decir que van a ver más &nbsp;“cosa u objeto” conectados a internet que personas.</div>', 'concepto', 'concepto.jpg', 2, NULL, '2015-10-30 03:21:10', '2015-10-30 03:21:10'),
(2, 'Interacion Maquina a Maquina', '<div>M2M (máquina a máquina) es un concepto genérico que se refiere al intercambio de información o comunicación en formato de datos entre dos máquinas remotas.</div><div>M2M es una forma de comunicación entre máquinas que permite agilizar los procesos, realizarlos de forma más eficiente y liberar a las personas de tareas tediosas o peligrosas.</div>', 'maquina-a-maquina', 'm2m.jpg', 1, NULL, '2015-10-30 03:22:22', '2015-10-30 03:22:22'),
(3, 'Seguridad en el Internet de las cosas', '<div><i>Se recomienda que los fabricantes de dispositivos conectados adopten tres medidas para lograr que los dispositivos sean menos vulnerables:</i></div><div><i>Implementar la seguridad desde el diseño del dispositivo mediante test de privacidad y un cifrado seguro.</i></div><div><i>Hacer que el dispositivo almacene solo la información necesaria.</i></div><div><i>Ser completamente transparentes con los consumidores para que sepan exactamente qué datos se van a utilizar y transmitir.</i></div>', 'seguridad', 'seguridad.jpg', 3, NULL, '2015-10-30 03:23:35', '2015-10-30 03:23:35'),
(4, 'IPV6 para el internet de las cosas', '<div><b>Los términos M2M e IoT, la Internet de las Cosas supone conectar a la red millones de nuevos dispositivos. Desde sencillos sensores o actuadores para manejar electrodomésticos, hasta complejas infraestructuras públicas de alumbrado o sistemas industriales, pasando por coches y casas inteligentes.</b></div><div><b>El internet se quedara sin direcciones IPv4 públicas, a pesar de esta limitación los negocios en línea han sido lentos en adoptar IPv6.&nbsp;</b></div>', 'ipv6', 'ipv6.jpg', 4, NULL, '2015-10-30 03:24:50', '2015-10-30 03:24:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_28_174608_create_articles_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `firstname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phrase` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `email`, `password`, `image`, `phrase`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'carlos', 'peñalba', 'javiier507', 'javiier507@correo.com', 'secret', 'perfil-carlos.jpg', ' Un reactor nuclear es como una mujer. Solo tienes que leer el manual y apretar los botones adecuados', 'gE7HrGB2xRKUHq7HzsgV2YKqnii57hwkETBKE7mg8EUaO9axN3x2IUu1NqcL', '0000-00-00 00:00:00', '2015-10-30 03:22:25'),
(2, 'kisna', 'quiroz', 'kisna', 'kisna@correo.com', '$2y$10$zNhNUrHc16htg6VXC9oqZuKAy7E5prc/w8tKK8gC0qfhw1Zmw0LDG', 'perfil-kisna.jpg', 'Cuando una mujer dice que nada anda mal, es por que todo anda mal y si dice que todo anda mal, es por que todo anda mal', '9Qmh5AkZ4qBjamVycqRDA1naXKANQFILBlWVrz3qay9R6wt6KxJ4gUqaxU2S', '0000-00-00 00:00:00', '2015-10-30 03:21:15'),
(3, 'christopher', 'miranda', 'christopher', 'christopher@correo.com', '$2y$10$9xTDAyp5PjQSzhco.21rh.kE5rtIuOYCEEY7LIC9saBDDYBdxpnXG', 'perfil-christopher.jpg', ' Tendrá todo el dinero del mundo, pero hay algo que nunca podrá comprar... un dinosaurio', '2aHtBZ8Xpeiq30QyfU8fCVsVftrIcPf0efkIiNJfsAVpOSVHy7Rx1wrxxDFa', '0000-00-00 00:00:00', '2015-10-30 03:23:40'),
(4, 'jorge', 'aguilar', 'jorge', 'jorge@correo.com', '$2y$10$5O2khQgNqtoJiKLKEmlLV.zzygkrrG4GrrqyiSK/Lv3UdVqs4cX8q', 'perfil-jorge.jpg', 'Durante toda mi vida he tenido un solo sueño: alcanzar todas mis metas', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `articles_title_unique` (`title`), ADD KEY `articles_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_username_unique` (`username`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articles`
--
ALTER TABLE `articles`
ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
