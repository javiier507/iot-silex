<?php
namespace Appsilex;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

require_once 'controllers/ArticleController.php';
require_once 'controllers/UserController.php';

//middleware
$auth = function (Request $request, Application $app)
{
	if(!$app['session']->has('user'))
    {
        return $app->redirect('/iniciar-sesion');
    }
};

$ok = function(Request $request, Application $app)
{
	if($app['session']->has('user'))
    {
        return $app->redirect('/administracion');
    }	
};

//blog
$app->get('/', 'Appsilex\ArticleController::index');
$app->get('/articulo/{id}', 'Appsilex\ArticleController::show');
$app->get('/equipo', 'Appsilex\UserController::index');
$app->get('/ubicacion', 'Appsilex\UserController::ubication');

//administracion
$app->get('/administracion', 'Appsilex\ArticleController::admin')->before($auth);
$app->get('/articulo-nuevo', 'Appsilex\ArticleController::create')->before($auth);
$app->post('/articulo-nuevo', 'Appsilex\ArticleController::store')->before($auth);
$app->delete('/articulo/{id}', 'Appsilex\ArticleController::destroy')->before($auth);

//autenticacion
$app->get('/iniciar-sesion', 'Appsilex\UserController::getLogin')->before($ok);
$app->post('/iniciar-sesion', 'Appsilex\UserController::postLogin')->before($ok);
$app->get('/salir', 'Appsilex\UserController::logout')->before($auth);