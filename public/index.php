<?php

require_once __DIR__.'/../vendor/autoload.php';
$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => __DIR__.'/../app/views']);
$app->register(new Silex\Provider\SessionServiceProvider());
//$app['debug'] = true;
require_once '../app/routes.php';
$app->run();